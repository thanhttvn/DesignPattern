/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategypatternexample;

/**
 *
 * @author Admin
 */
public class Context {

    private Strategy strategy;
    int counter;

    public Context() {
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
    
    public void method1(){
        counter++;
    }
    

    public int executeStrategy(int num1, int num2) {
        return strategy.doOperation(num1, num2);
    }
}
